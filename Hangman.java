import java.util.Scanner;
public class Hangman{

//FUNC
	public static int isLetterInWord(String word, char c) {
		for (int i =0; i<4 ; i++){
			if (word.charAt(i)==c){
				return i;
			}
		}
		//returns position of letter is present
	   return -1;
	   //if letter not present => -1
	}


//FUNC
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
	}


//FUNC
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
		String newWord = "";
		boolean[] letterAgain = {letter0, letter1, letter2, letter3};

		for(int i = 0; i<4; i++){
			if(letterAgain[i] == true){
				newWord += word.charAt(i);
			}
			else{
				newWord = newWord + " _ ";
			}
		}
		System.out.println("Your result is "+ newWord);
	}


//FUNC
	public static  void runGame(String word){
		boolean[] letter = {false, false, false, false};
		int numberOfGuess = 0;
	
		while(numberOfGuess < 6 && !(letter[0] && letter[1] && letter[2] && letter[3])){

			Scanner reader = new Scanner (System.in);
			
			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess =toUpperCase(guess);

			for(int i=0; i<4; i++){
				if(isLetterInWord(word, guess) == i){
					letter[i] = true;
				}
			}
			
            if(isLetterInWord(word, guess)==-1){
				numberOfGuess++;
			}
			
			printWork(word, letter[0], letter[1], letter[2], letter[3]);
		}
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		if(letter[0] && letter[1] && letter[2] && letter[3]){
		System.out.println("Congrats! You got it :)");
		}
	}
}