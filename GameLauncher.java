import java.util.Scanner;

public class GameLauncher{
	
	//main - calls gameOfChoice and runs the asked for game
	public static void main(String[] args){
			runWhole();
	}
	
	//keeps looping the program as many times as the user wants - takes the input to choose the game to play - runs the game and takes input again to ask the user if they want to play again. If yes - comes back to the top. Otherwise termintes the program 
	public static void runWhole(){
		Scanner reader = new Scanner(System.in);
		boolean keepItRunning = true;
		
		while(keepItRunning){
			boolean acceptRedo = true; 
			int replayInput = 3;
			
			int gameOfChoice = gameSelectionInput();		
			if(gameOfChoice == 1){
				hangManRunner();			
			}
			if(gameOfChoice == 2){
				wordleRunner();
			}
			
			while(acceptRedo){
				System.out.println("If you'd like to play again - press 1. Otherwise press 0");
				replayInput = reader.nextInt();
				if(replayInput == 1 || replayInput == 0){
				acceptRedo = false;
				}
				if(replayInput == 0){
					keepItRunning = false;
					System.out.println("Alright - Goodbye :)");
				}
			}
			
		}
	}
	
	//asks for user input and loops until the user inputs a 1 or a 2
	public static int gameSelectionInput(){
		Scanner reader = new Scanner(System.in);
		boolean numberAccept = true;
		int gameSelection = 3;
		
		System.out.println("Hello!");
		
		while(numberAccept){
			System.out.println("If you would like to play Hangman - press 1. If you would like to play wordle - press 2. I won't stop asking until it's a 1 or a 2.");
			gameSelection = reader.nextInt();
			if(gameSelection == 1 || gameSelection == 2){
				numberAccept = false;
			}
		}
		return gameSelection;
	}
	
	//runs hangman when called
	public static void hangManRunner(){
		Scanner reader = new Scanner (System.in);
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		word = word.toUpperCase();
	    Hangman.runGame(word);	
	}
	
	//runs wordle when called
	public static void wordleRunner(){
		String answer = Wordle.generateWord();
		Wordle.runGame(answer);
	}
}
