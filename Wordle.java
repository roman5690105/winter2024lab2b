import java.util.Scanner;	
import java.util.Random;

public class Wordle {
	
	//Chooses a random word from the array of 20 words to be used as an answer to the game
	public static String generateWord(){
		Random wordRandomizer = new Random(); 
		String[] wordList = new String[] {"BREAD", "GHOST", "ADULT", "CHIVE", "GAUZE", "EARTH", "SCENT", "LYMPH", "KNACK", "SMILE", "MAGIC", "SAUTE", "PLAID", "GUARD", "JOKER", "FLOAT", "VEXED", "STYLE", "FROST", "MAPLE"};
		int x = wordRandomizer.nextInt(wordList.length);
		return wordList[x];
	}
	//Checks if letter exists somewhere in the word (but not in same postition)
	public static boolean letterInWord(String word, char letter){
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == letter){
				return true;
			}
		}
		return false;
	}
	//Checks if letter is contained in word and in position
	public static boolean letterInSlot(String word, char letter, int position){
		for(int i = 0; i < word.length(); i++){
			if(letterInWord(word, letter) == true && word.charAt(position) == letter){
				return true;
			}
		}
		return false;
	}
	//Checks what color should be shown per char depending on the letter guessed
	public static String[] guessWord(String answer, String guess){
		String[] colorArr = new String[5];
		for(int i = 0; i < colorArr.length; i++){
			if(letterInSlot(answer, guess.charAt(i), i)){
				colorArr[i] = "green";
			}
			else if(letterInWord(answer, guess.charAt(i))){
				colorArr[i] = "yellow";
			}
			else{
				colorArr[i] = "white";
			}
		}
		return colorArr;
	}
	//Print chars w/ set colors based on array 
	public static void presentResults(String word, String[] colors){
		String[] printColor = new String[5];
		
		for(int i = 0; i < colors.length; i++){
			if(colors[i] == "green"){
				printColor[i] = "\u001B[32m"; 
			}
			else if(colors[i] == "yellow"){
				printColor[i] = "\u001B[33m";
			}
			else{
				printColor[i] = "\u001B[0m";
			}
		}
		System.out.println(printColor[0] + word.charAt(0) + "" + printColor[1]  + word.charAt(1) + ""  + printColor[2] + word.charAt(2) + "" + printColor[3]  + word.charAt(3) + "" + printColor[4]  + word.charAt(4) + "\u001B[0m");
	}
	//Asks user for guess and checks if it a 5 letter word
	public static String readGuess(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Input your guess:");
		String guess = reader.nextLine();
		if(guess.length() != 5){
			System.out.println("Invalid guess, must be 5 letters in length");
			return readGuess();
		}
		return guess.toUpperCase();
	}
	//Main game loop that the user interacts with
	public static void runGame(String targetWord){
		int maxTries = 6;
		boolean isWinner = false;
		
		//Loops 6 times to see if you are right or wrong & returns lose state after 6th loop
		while(maxTries > 0 && isWinner == false){
			String guess = readGuess();
			presentResults(guess, guessWord(targetWord, guess));
			
			if(targetWord.equals(guess)){
				isWinner = true;
				System.out.println("You Win!");
			}
			else{
				maxTries--;
			}
		}
		if(maxTries <= 0){
			System.out.println("Try Again, The word was " + targetWord);
		}
	}
	//Main method
	public static void main(String[] arg){
		
		String answer = generateWord();
		runGame(answer);
	}
}